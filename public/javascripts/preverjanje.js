/* global $ */
$('#dodajKomentar').submit(function(dogodek) {
  $('.alert.alert-danger').hide();
  if (!$('input#naziv').val() || !$('select#ocena').val() || !$('textarea#komentar').val()) {
    if ($('.alert.alert-danger').length) {
      $('.alert.alert-danger').show();
    } else {
      $(this).prepend('<div role="alert" class="alert alert-danger">' + 
        'Prosim izpolnite vsa vnosna polja!' + '</div>');
    }
    return false;
  }
});