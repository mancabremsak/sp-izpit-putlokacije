var mongoose = require('mongoose');
var Lokacija = mongoose.model('Lokacija');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.lokacijeSeznamPoRazdalji = function(zahteva, odgovor) {
  var lng = parseFloat(zahteva.query.lng);
  var lat = parseFloat(zahteva.query.lat);
  var razdalja = parseFloat(zahteva.query.maxRazdalja);
  razdalja = isNaN(razdalja) ? 20 : razdalja;
  var stZadetkov = 10;
  if ((!lng && lng !== 0) || (!lat && lat !== 0)) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": "Parametra lng in lat sta obvezna."
    });
    return;
  }
  Lokacija
    .aggregate([{
      $geoNear: {
        near: {
          type: "Point",
          coordinates: [lng, lat]
        },
        maxDistance: razdalja * 1000,
        key: "coords",
        distanceField: "razdalja",
        spherical: true,
        query: "find()"
      }
    }])
    .limit(stZadetkov)
    .exec(function(napaka, rezultati) {
      var lokacije = [];
      if (napaka) {
        vrniJsonOdgovor(odgovor, 500, napaka);
      } else {
        rezultati.forEach(function(dokument) {
          lokacije.push({
            razdalja: dokument.razdalja / 1000,
            naziv: dokument.naziv,
            naslov: dokument.naslov,
            ocena: dokument.ocena,
            lastnosti: dokument.lastnosti,
            _id: dokument._id
          });
        });
        vrniJsonOdgovor(odgovor, 200, lokacije);
      }
    });
};

module.exports.lokacijeKreiraj = function(zahteva, odgovor) {
  Lokacija.create({
    naziv: zahteva.body.naziv,
    naslov: zahteva.body.naslov,
    lastnosti: zahteva.body.lastnosti.split(","),
    koordinate: [
      parseFloat(zahteva.body.lng), 
      parseFloat(zahteva.body.lat)
    ],
    delovniCas: [{
      dnevi: zahteva.body.dnevi1,
      odprtje: zahteva.body.odprtje1,
      zaprtje: zahteva.body.zaprtje1,
      zaprto: zahteva.body.zaprto1
    }, {
      dnevi: zahteva.body.dnevi2,
      odprtje: zahteva.body.odprtje2,
      zaprtje: zahteva.body.zaprtje2,
      zaprto: zahteva.body.zaprto2
    }]
  }, function(napaka, lokacija) {
    if (napaka) {
      vrniJsonOdgovor(odgovor, 400, napaka);
    } else {
      vrniJsonOdgovor(odgovor, 201, lokacija);
    }
  });
};

module.exports.lokacijePreberiIzbrano = function(zahteva, odgovor) {
  if (zahteva.params && zahteva.params.idLokacije) {
    if (!(/^\w+$/.test(zahteva.params.idLokacije)) || Object.keys(zahteva.query).length > 0) {
      vrniJsonOdgovor(odgovor, 400, {
        "sporočilo": "Napačna zahteva!"
      });
      return;
    }
    Lokacija
      .findById(zahteva.params.idLokacije)
      .exec(function(napaka, lokacija) {
        if (!lokacija) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": 
              "Ne najdem lokacije s podanim enoličnim identifikatorjem idLokacije."
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        vrniJsonOdgovor(odgovor, 200, lokacija);
      });
  } else {
    vrniJsonOdgovor(odgovor, 400, { 
      "sporočilo": "Manjka enolični identifikator idLokacije!"
    });
  }
};

module.exports.lokacijePosodobiIzbrano = function(zahteva, odgovor) {
  if (!zahteva.params.idLokacije) {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem lokacije, idLokacije je obvezen parameter"
    });
    return;
  }
  Lokacija
    .findById(zahteva.params.idLokacije)
    .select('-komentarji -ocena')
    .exec(
      function(napaka, lokacija) {
        if (!lokacija) {
          vrniJsonOdgovor(odgovor, 404, {
            "sporočilo": "Ne najdem lokacije."
          });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 500, napaka);
          return;
        }
        lokacija.naziv = zahteva.body.naziv;
        lokacija.naslov = zahteva.body.naslov;
        lokacija.lastnosti = zahteva.body.lastnosti.split(",");
        lokacija.koordinate = [
          parseFloat(zahteva.body.lng), 
          parseFloat(zahteva.body.lat)
        ],
        lokacija.delovniCas = [{
          dnevi: zahteva.body.dnevi1,
          odprtje: zahteva.body.odprtje1,
          zaprtje: zahteva.body.zaprtje1,
          zaprto: zahteva.body.zaprto1
        }, {
          dnevi: zahteva.body.dnevi2,
          odprtje: zahteva.body.odprtje2,
          zaprtje: zahteva.body.zaprtje2,
          zaprto: zahteva.body.zaprto2
        }];
        lokacija.save(function(napaka, lokacija) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 400, napaka);
          } else {
            vrniJsonOdgovor(odgovor, 200, lokacija);
          }
        });
      }
    );
};


module.exports.lokacijeIzbrisiIzbrano = function(zahteva, odgovor) {
  var idLokacije = zahteva.params.idLokacije;
  if (idLokacije) {
    Lokacija
      .findByIdAndRemove(idLokacije)
      .exec(
        function(napaka, lokacija) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
          }
          vrniJsonOdgovor(odgovor, 204, null);
        }
      );
  } else {
    vrniJsonOdgovor(odgovor, 400, {
      "sporočilo": 
        "Ne najdem lokacije, idLokacije je obvezen parameter."
    });
  }
};

module.exports.dodajTestne = function(zahteva, odgovor) {
  Lokacija.create({
    naziv : "Stari grad Celje",
    naslov : "Cesta na grad 78, 3000 Celje, Slovenija",
    ocena : 3,
    lastnosti : [ "slikovit razgled", "vstopnina", "otrokom prijazno" ],
    koordinate : [ 15.271601, 46.219849 ],
    delovniCas: [{
                   dnevi: "ponedeljek - petek",
                   odprtje: "9:00",
                   zaprtje: "21:00",
                   zaprto: false
                  }, {
                   dnevi: "sobota",
                   odprtje: "9:00",
                   zaprtje: "19:00",
                   zaprto: false
                  }, {
                   dnevi: "nedelja",
                   zaprto: true
                  }],
    komentarji: [{
                  avtor: "Dejan Lavbič",
                  ocena: 5,
                  datum: Date("2018-11-07T00:00:00Z"),
                  besediloKomentarja: "Odlična lokacija, ne morem je prehvaliti."
                }, {
                  avtor: "Kim Jong Un",
                  ocena: 1,
                  datum: Date("2018-11-08T00:00:00Z"),
                  besediloKomentarja: "Čisti dolgčas, še kava je zanič."
                }]
}, {
  naziv : "ZOO Ljubljana",
  naslov : "Večna pot 70, 1000 Ljubljana, Slovenija",
  ocena : 4,
  lastnosti : [ "priporočljivo za otroke", "živali", "parkirišče je na voljo", "vstopnina" ],
  koordinate: [ 14.472072, 46.052747 ],
  delovniCas: [{
                  dnevi : "ponedeljek - nedelja",
                  odprtje : "9:00",
                  zaprtje : "19:00",
                  zaprto : false
                }],
  komentarji : [{
                  avtor : "Tinček Sosedov",
                  ocena : 5,
                  datum : Date("2018-07-17T00:00:00Z"),
                  besediloKomentarja : "Otroci so bili navdušeni nad igrišči, ki so resnično izvirna. Lahko bi ostali cel dan."
                }, {
                  avtor : "Marija Kovačeva",
                  ocena : 3,
                  datum : Date("2018-07-10T00:00:00Z"),
                  besediloKomentarja : "Všeč so mi bili morski levi in tigri, medtem ko mi je slon deloval osamljeno."
                }]
}, {
  naziv : "Angkor Wat",
  naslov : "Krong Siem Reap, Kambodža",
  ocena : 4,
  lastnosti : [ "vstopnina", "slikovit razgled", "turistična atrakcija" ],
  koordinate : [ 103.866667, 13.4125 ],
  delovniCas : [{
                  dnevi : "ponedeljek - nedelja",
                  odprtje : "5:00",
                  zaprtje : "17:00",
                  zaprto : false
                }],
  komentarji : [{
                  avtor : "Janez Kranjski",
                  ocena : 3,
                  datum : Date("2018-08-20T00:00:00Z"),
                  besediloKomentarja : "Cena vstopnice se je v zadnjih letih povišala, saj dnevni vstop stane kar $37. Zelo zanimiva lokacija, vendar je pogosto polna turistov."
                }, {
                  avtor: "Miško Rokohitri",
                  ocena: 5,
                  datum: Date("2018-08-20T00:00:00Z"),
                  besediloKomentarja: "To preprosto morate videti - ogromen kompleks templov in ena najbolj razpoznavnih atrakcij na svetu."
                }],
  }, function(napaka, uporabnik) {
    if (napaka) {
      vrniJsonOdgovor(odgovor, 400, napaka);
    } else {
      vrniJsonOdgovor(odgovor, 201, { "status": "uspešno" });
    }
    });
};

module.exports.izbrisiVse = function(zahteva, odgovor) {
  Lokacija.deleteMany({}, function(napaka) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
          }
          vrniJsonOdgovor(odgovor, 204, { "status": "uspešno" });
        }
    );
};