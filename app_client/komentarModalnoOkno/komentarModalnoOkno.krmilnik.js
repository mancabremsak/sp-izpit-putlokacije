(function() {
  function komentarModalnoOkno($uibModalInstance, edugeocachePodatki, podrobnostiLokacije) {
    var vm = this;

    vm.podrobnostiLokacije = podrobnostiLokacije;

    vm.modalnoOkno = {
      preklici: function() {
        $uibModalInstance.close();
      },
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };

    vm.posiljanjePodatkov = function() {
      vm.napakaNaObrazcu = "";
      if (!vm.podatkiObrazca.ocena || !vm.podatkiObrazca.komentar) {
        vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja!";
        return false;
      } else {
        vm.dodajKomentar(vm.podrobnostiLokacije.idLokacije, vm.podatkiObrazca);
      }
    };

    vm.dodajKomentar = function(idLokacije, podatkiObrazca) {
      edugeocachePodatki.dodajKomentarZaId(idLokacije, {
        ocena: podatkiObrazca.ocena,
        komentar: podatkiObrazca.komentar
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        },
        function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri shranjevanju komentarja, poskusite znova!";
        }
      );
    };
  }
  komentarModalnoOkno.$inject = ['$uibModalInstance', 'edugeocachePodatki', 'podrobnostiLokacije'];

  /* global angular */
  angular
    .module('edugeocache')
    .controller('komentarModalnoOkno', komentarModalnoOkno);
})();