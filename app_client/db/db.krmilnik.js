(function() {
  function dbCtrl($scope, edugeocachePodatki) {
    var vm = this;
    
    console.log("sem v db");
    
    vm.napolniTestne = function() {
      edugeocachePodatki.vnosTestnih();
    };
    
    vm.izbrisiTestne = function() {
      edugeocachePodatki.izbrisTestnih();
    };
    
  }
  
  dbCtrl.$inject = ['$scope', 'edugeocachePodatki'];
  /* global angular */
  angular
    .module('edugeocache')
    .controller('dbCtrl', dbCtrl);
})();
