(function() {
  function seznamCtrl($scope, edugeocachePodatki, geolokacija) {
    var vm = this;
    vm.glavaStrani = {
      naslov: 'EduGeoCache',
      podnaslov: 'Poiščite zanimive lokacije blizu vas!'
    };
    vm.stranskaOrodnaVrstica = 'Iščete lokacijo za kratkočasenje? EduGeoCache vam pomaga pri iskanju zanimivih lokacij v bližini. Mogoče imate kakšne posebne želje? Naj vam EduGeoCache pomaga pri iskanju bližnjih zanimivih lokacij.';
    
    vm.sporocilo = "Pridobivam trenutno lokacijo.";
    
    vm.napolniTestne = function() {
      edugeocachePodatki.vnosTestnih();
    };
    
    vm.izbrisiTestne = function() {
      edugeocachePodatki.izbrisTestnih();
    };
    
    vm.pridobiPodatke = function(lokacija) {
      var lat = lokacija.coords.latitude,
          lng = lokacija.coords.longitude;
      vm.sporocilo = "Iščem bližnje lokacije.";
      edugeocachePodatki.koordinateTrenutneLokacije(lat, lng).then(
        function success(odgovor) {
          vm.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem lokacij.";
          vm.data = {
            lokacije: odgovor.data
          };
        }, function error(odgovor) {
          vm.sporocilo = "Prišlo je do napake!";
          console.log(odgovor.e);
        }
      );    
    };
    
    vm.prikaziNapako = function(napaka) {
      $scope.$apply(function() {
        vm.sporocilo = napaka.message;
      });
    };
    
    vm.niLokacije = function() {
      $scope.$apply(function() {
        vm.sporocilo = "Vaš brskalnik ne podpira geolociranja!";
      });
    };
    
    geolokacija.vrniLokacijo(
      vm.pridobiPodatke, 
      vm.prikaziNapako, 
      vm.niLokacije);
  }
  seznamCtrl.$inject = ['$scope', 'edugeocachePodatki', 'geolokacija'];
  
  /* global angular */
  angular
    .module('edugeocache')
    .controller('seznamCtrl', seznamCtrl);
})();