(function() {
  var edugeocachePodatki = function($http, avtentikacija) {
    var koordinateTrenutneLokacije = function(lat, lng) {
      return $http.get(
        'api/lokacije?lng=' + lng /* '14.469027' */ + 
          '&lat=' + lat /* '46.050129' */ + 
          '&maxRazdalja=100');
    };
    var podrobnostiLokacijeZaId = function(idLokacije) {
      return $http.get('/api/lokacije/' + idLokacije);
    };
    var dodajKomentarZaId = function(idLokacije, podatki) {
      return $http.post('/api/lokacije/' + idLokacije + '/komentarji', podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    
    var lokacijePosodobiIzbrano = function(idLokacije, podatki) {
      return $http.post('/api/lokacije/' + idLokacije, podatki);
    };
    
    var vnosTestnih = function() {
      return $http.post('/api/db').then(
        function success(odgovor) {
          alert('Vnos uspešen');
        });
    };
    
    var izbrisTestnih = function() {
      return $http({
        method: 'DELETE',
        url: 'api/db/'
      }).then(
        function success(odgovor) {
          alert('Uspešno izbrisano');
        });
    };
    return {
      koordinateTrenutneLokacije: koordinateTrenutneLokacije,
      podrobnostiLokacijeZaId: podrobnostiLokacijeZaId,
      dodajKomentarZaId: dodajKomentarZaId,
      vnosTestnih: vnosTestnih,
      izbrisTestnih: izbrisTestnih,
      lokacijePosodobiIzbrano: lokacijePosodobiIzbrano
    };
  };
  edugeocachePodatki.$inject = ['$http', 'avtentikacija'];
  
  /* global angular */
  angular
    .module('edugeocache')
    .service('edugeocachePodatki', edugeocachePodatki);
})();