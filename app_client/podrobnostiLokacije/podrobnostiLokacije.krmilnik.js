(function() {
  function podrobnostiLokacijeCtrl($routeParams, $location, $uibModal, edugeocachePodatki, avtentikacija) {
    var vm = this;

    vm.idLokacije = $routeParams.idLokacije;
    
    vm.jePrijavljen = avtentikacija.jePrijavljen();
    
    vm.prvotnaStran = $location.path();
    
    vm.prikaziPutLokacije = function() {
      var putLokacijeModalnoOkno = $uibModal.open({
        templateUrl: '/putLokacije/putLokacije.pogled.html',
        controller: 'putLokacije',
        controllerAs: 'vm',
        resolve: {
        podrobnostiLokacije: function() {
        return {
          idLokacije: vm.idLokacije,
          nazivLokacije: vm.podatki.lokacija.naziv,
          /*naslovLokacije: vm.podatki.lokacija.naslov,
          lng: vm.podatki.lokacija.koordinate[0],
          lat: vm.podatki.lokacija.koordinate[1],
          lastnosti: vm.podatki.lokacija.lastnosti[0] //idk
          */
        };
        }
      }
      });
      
      putLokacijeModalnoOkno.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          vm.podatki.lokacija.naziv.push(podatki);
      }, function(napaka) {
        // Ulovi dogodek in ne naredi ničesar
      });
    };

    vm.prikaziPojavnoOknoObrazca = function() {
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/komentarModalnoOkno/komentarModalnoOkno.pogled.html',
        controller: 'komentarModalnoOkno',
        controllerAs: 'vm',
        resolve: {
          podrobnostiLokacije: function() {
            return {
              idLokacije: vm.idLokacije,
              nazivLokacije: vm.podatki.lokacija.naziv
            };
          }
        }
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          vm.podatki.lokacija.komentarji.push(podatki);
      }, function(napaka) {
        // Ulovi dogodek in ne naredi ničesar
      });
    };
    
    edugeocachePodatki.podrobnostiLokacijeZaId(vm.idLokacije).then(
      function success(odgovor) {
        vm.podatki = { lokacija: odgovor.data };
        vm.glavaStrani = {
          naslov: vm.podatki.lokacija.naziv
        };
      },
      function error(odgovor) {
        console.log(odgovor.e);
      }
    );
  }
  podrobnostiLokacijeCtrl.$inject = ['$routeParams', '$location', '$uibModal', 'edugeocachePodatki', 'avtentikacija'];
  
  /* global angular */
  angular
    .module('edugeocache')
    .controller('podrobnostiLokacijeCtrl', podrobnostiLokacijeCtrl);
})();