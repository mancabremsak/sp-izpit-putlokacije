(function() {
  function putLokacije($uibModalInstance, edugeocachePodatki, podrobnostiLokacije) {
    var vm = this;
    
    vm.podrobnostiLokacijePUT = podrobnostiLokacije;

    vm.modalnoOknoPUT = {
      preklici: function() {
        $uibModalInstance.close();
      },
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    vm.posiljanjePodatkovPUT = function() {
      vm.napakaNaObrazcu = "";
      if (!vm.prijavniPodatki.naziv) {
        vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja!";
        return false;
      } else {
        vm.urediLokacijo(vm.podrobnostiLokacijePUT.idLokacije, vm.prijavniPodatki);
        /*console.log(vm.prijavniPodatki);
        return false;*/
      }
    };
    
    vm.urediLokacijo = function(idLokacije, prijavniPodatki) {
      edugeocachePodatki.lokacijePosodobiIzbrano(idLokacije, {
        naziv: 'Ljubljanski grad'
      }).then(
        function success(odgovor) {
          alert('bravo!!');
          vm.modalnoOknoPUT.zapri(odgovor.data);
        },
        function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri shranjevanju komentarja, poskusite znova!";
        }
      );
    };
  }
  putLokacije.$inject = ['$uibModalInstance', 'edugeocachePodatki', 'podrobnostiLokacije'];

  /* global angular */
  angular
    .module('edugeocache')
    .controller('putLokacije', putLokacije);
})();