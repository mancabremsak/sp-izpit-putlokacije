var request = require('request');
var apiParametri = {
  streznik: 'http://localhost:' + process.env.PORT
};
if (process.env.NODE_ENV === 'production') {
  apiParametri.streznik = 'https://edugeocache-sp-2018-2019.herokuapp.com/';
}

var prikaziZacetniSeznam = function(req, res, vsebina) {
  res.render('lokacije-seznam', {
    title: 'EduGeoCache - Poiščite zanimive lokacije blizu vas!',
    glavaStrani: {
      naslov: 'EduGeoCache',
      podnaslov: 'Poiščite zanimive lokacije blizu vas!'
    },
    stranskaOrodnaVrstica: 'Iščete lokacijo za kratkočasenje? EduGeoCache vam pomaga pri iskanju zanimivih lokacij v bližini. Mogoče imate kakšne posebne želje? Naj vam EduGeoCache pomaga pri iskanju bližnjih zanimivih lokacij.'
  });  
};

var prikaziPodrobnostiLokacije = function(req, res, podrobnosti) {
  res.render('lokacija-podrobnosti', { 
    title: podrobnosti.naziv,
    glavaStrani: {
      naslov: podrobnosti.naziv
    },
    stranskaOrodnaVrstica: {
      kontekst: 'je na EduGeoCache, ker je zanimiva lokacija, ki si jo lahko ogledate, ko ste brez idej za kratek izlet.',
      poziv: 'Če vam je lokacija všeč, ali pa tudi ne, dodajte svoj komentar in s tem pomagajte ostalim uporabnikom pri odločitvi.'
    },
    lokacija: podrobnosti
  });  
};

var prikaziObrazecZaKomentar = function(req, res, podrobnostiLokacije) {
  res.render('lokacija-nov-komentar', {
    title: 'Dodaj komentar za ' + podrobnostiLokacije.naziv + ' na EduGeoCache',
    glavaStrani: {
      naslov: 'Komentiraj ' + podrobnostiLokacije.naziv
    },
    napaka: req.query.napaka,
    url: req.originalUrl
  });
};

var pridobiPodrobnostiLokacije = function(req, res, callback) {
  var pot = '/api/lokacije/' + req.params.idLokacije;
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'GET',
    json: {}
  };
  request(
    parametriZahteve,
    function(napaka, odgovor, vsebina) {
      if (odgovor.statusCode === 200) {
        vsebina.koordinate = {
          lng: vsebina.koordinate[0],
          lat: vsebina.koordinate[1]
        };
        callback(req, res, vsebina);
      } else {
        prikaziNapako(req, res, odgovor.statusCode);
      }
    }
  );  
};

/* Vrni začetno stran s seznamom lokacij */
module.exports.seznam = function(req, res) {
  prikaziZacetniSeznam(req, res);
};

/* Vrni podrobnosti lokacije */
module.exports.podrobnostiLokacije = function(req, res) {
  pridobiPodrobnostiLokacije(req, res, function(callback_req, callback_res, vsebina) {
    prikaziPodrobnostiLokacije(callback_req, callback_res, vsebina);
  });
};

/* Vrni stran za dodajanje komentarjev */
module.exports.dodajKomentar = function(req, res) {
  pridobiPodrobnostiLokacije(req, res, function(callback_req, callback_res, vsebina) {
    prikaziObrazecZaKomentar(callback_req, callback_res, vsebina);
  });
};

/* Shrani komentar na strežnik */
module.exports.shraniKomentar = function(req, res) {
  var idLokacije = req.params.idLokacije;
  var pot = '/api/lokacije/' + idLokacije + '/komentarji';
  var posredovaniPodatki = {
    naziv: req.body.naziv,
    ocena: req.body.ocena,
    komentar: req.body.komentar
  };
  var parametriZahteve = {
    url: apiParametri.streznik + pot,
    method: 'POST',
    json: posredovaniPodatki
  };
  if (!posredovaniPodatki.naziv || !posredovaniPodatki.ocena || !posredovaniPodatki.komentar) {
    res.redirect('/lokacija/' + idLokacije + '/komentar/nov?napaka=vrednost');
  } else {
    request(
      parametriZahteve,
      function(napaka, odgovor, vsebina) {
        if (odgovor.statusCode === 201) {
          res.redirect('/lokacija/' + idLokacije);
        } else if (odgovor.statusCode === 400 && 
          vsebina.name && vsebina.name === "ValidationError") {
            res.redirect('/lokacija/' + idLokacije + '/komentar/nov?napaka=vrednost');
        } else {
          prikaziNapako(req, res, odgovor.statusCode);
        }
      }
    );
  }
};

var formatirajRazdaljo = function(razdalja) {
  var vrednostRazdalje, enota;
  if (razdalja > 1) {
    vrednostRazdalje = parseFloat(razdalja).toFixed(1);
    enota = 'km';
  } else {
    vrednostRazdalje = parseInt(razdalja * 1000, 10);
    enota = 'm';
  }
  return vrednostRazdalje + enota;
};

var prikaziNapako = function(req, res, statusCode) {
  var naslov, vsebina;
  if (statusCode == 404) {
    naslov = "404, strani ni mogoče najti.";
    vsebina = "Hmm, kako je to mogoče? Nekaj je šlo narobe.";
  } else {
    naslov = statusCode + ", nekaj je šlo narobe.";
    vsebina = "Nekaj nekje očitno ne deluje.";
  }
  res.status(statusCode);
  res.render('genericno-besedilo', {
    title: naslov,
    vsebina: vsebina
  });
};